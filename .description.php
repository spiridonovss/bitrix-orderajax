<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main\Localization\Loc;

$arComponentDescription = [
	"NAME" => Loc::getMessage("GOODS_LIST_COMPONENT"),
	"DESCRIPTION" => Loc::getMessage("GOODS_LIST_COMPONENT_DESCRIPTION"),
    "CACHE_PATH" => "Y",
	"PATH" => [
        "ID" => Loc::getMessage("GOODS_LIST_COMPONENT_PATH_ID"),
        "NAME" => Loc::getMessage("GOODS_LIST_COMPONENT_PATH_NAME"),
	]
];