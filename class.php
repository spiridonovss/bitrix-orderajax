<?  if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

use  Bitrix\Main\Loader,
     Bitrix\Main\SystemException,
     Bitrix\Sale,
     Bitrix\Main\Context,
     Bitrix\Sale\Order;



class OptovikOrderAjax extends CBitrixComponent
{

    /**
     * @var bool
     */
    public $order;
    public $request;
    public $arResult;
    public $arRequest; // возвращаем в обЪект json callback

    /**
     * @var bool
     */
    protected $errors = [];


    /**
     * ItiparkGoodsList constructor.
     */
    function __construct($component = null)
    {
        parent::__construct($component);

        if(!Loader::includeModule('sale')){
            $this->errors[] = 'No sale module';
        };

        if(!Loader::includeModule('catalog')){
            $this->errors[] = 'No catalog module';
        };
        parent::__construct();

        $this->request = $this->getRequest();
    }


    /**
     * Сборка arResult ? template
     */
    function executeComponent()
    {
        if($this->startResultCache())
        {
            try
            {
                $this->arResult = $this->createVirtualOrder();

                if (isset($this->request['save']) && $this->request['save'] == 'Y') {
                    $this->order->save();
                }

                $this->includeComponentTemplate();
            }
            catch (SystemException $e)
            {
                ShowError($e->getMessage());
            }
        }
    }


    /** POST - GET - данные (вспомогательный метод)
     * возвращает массив post-get-данных 
     * @return array
     */
    protected function getRequest() {
        $arRequest = [];

        $context = Context::getCurrent();
        $request = $context->getRequest();
        $method = $request->getRequestMethod();

        if ($method == "GET") {
            $arRequest = $request->getQueryList()->toArray();
        }
        if ($request->isPost()) {
           $arRequest = $request->getPostList()->toArray();
        }
        return $arRequest;
    }


    /**
     * @param $arParams
     * @return mixed
     */
    function onPrepareComponentParams($arParams)
    {
        // Можем указать в параметрах компонента принудительно тип плательщика Физ - Юр лицо
        if (isset($arParams['PERSON_TYPE_ID']) && intval($arParams['PERSON_TYPE_ID']) > 0) {
            $arParams['PERSON_TYPE_ID'] = intval($arParams['PERSON_TYPE_ID']);
        } else { // Если  не выбран Тип плательщика - ставим  по-умолчанию 1 (Физ.лицо)
            if (intval($this->request['PERSON_TYPE_ID']) > 0) {
                $arParams['PERSON_TYPE_ID'] = intval($this->request['PERSON_TYPE_ID']);
            } else {
                $arParams['PERSON_TYPE_ID'] = 1;
            }
        }
        return $arParams;
    }


    /** Создание Заказа и помещение в него Корзины
     * @param $registeredUserID
     */
    protected function setOrderInBasket($registeredUserID)
    {
        $this->order = Order::create(Bitrix\Main\Context::getCurrent()->getSite(), $registeredUserID);
        $this->order->setPersonTypeId($this->arResult['SELECTED_USER_TYPE']);

        // Получим корзину для текущего Юзера
        $basket = Bitrix\Sale\Basket::loadItemsForFUser(Bitrix\Sale\Fuser::getId(), Bitrix\Main\Context::getCurrent()->getSite());
        $price = $basket->getPrice();// Стоимость корзины с учетом скидок

        //получить количество и сумму товаров в корзине текущего юзера (БЕЗ отложенных):
        $result = Sale\Internals\BasketTable::getList(array(
        'filter' => array(
            'FUSER_ID' => Sale\Fuser::getId(),
            'ORDER_ID' => null,
            'LID' => SITE_ID,
            'CAN_BUY' => 'Y',
            'DELAY' => 'N',
        ),
        'select' => array('BASKET_COUNT', 'BASKET_SUM'),
        'runtime' => array(
            new \Bitrix\Main\Entity\ExpressionField('BASKET_COUNT', 'COUNT(*)'),
            new \Bitrix\Main\Entity\ExpressionField('BASKET_SUM', 'SUM(PRICE*QUANTITY)'),
        )
        ))->fetch();

        // для блока общего подсчета
        $cart_data  = CCommon::getCartData();
        $this->arResult['BASKET_CNT'] = CCommon::formatPrice($result['BASKET_COUNT']);
        $this->arResult['PRICE'] = formatPrice($result['BASKET_SUM']);
        $this->arResult['FULL_PRICE'] = formatPrice($result['BASKET_SUM']);

        // Редирект на каталог
        if ( $result['BASKET_SUM'] == 0 ) {
            // Проверим есть ли request ID order
            if ( intval($this->request['order_id']) > 0) {
                $arItemOrder = CSaleOrder::GetList(array(), array("ID" => $this->request['order_id']))->Fetch(); // ID заказа из переменной
                $this->arResult['LAST_ORDER'] = $arItemOrder;
            }
            else {
                LocalRedirect("/catalog/");
            }
        }
        $this->order->setBasket($basket);
    }


    /**  Конфигурация Типа пользователя и Формирование Result
     * @return mixed
     */
    protected function setPersonType4Result()
    {
        if ( !empty($this->request['PERSON_TYPE_ID']) &&  $this->request['PERSON_TYPE_ID'] > 0) {
            $this->arResult['SELECTED_USER_TYPE'] = $this->request['PERSON_TYPE_ID'];
        }
        else {
            $this->arResult['SELECTED_USER_TYPE'] = $this->arParams['PERSON_TYPE_ID'];
        }
        // Выведем переключатели для выбора типа плательщика для текущего сайта
        $db_ptype = CSalePersonType::GetList(Array("SORT" => "ASC"), Array("LID"=>Bitrix\Main\Context::getCurrent()->getSite(), "ACTIVE"=>"Y"));
        while ($ptype = $db_ptype->Fetch())
        {
            if ( $ptype['ID'] == $this->arResult['SELECTED_USER_TYPE'] ) {
                $ptype['SELECTED'] = "Y"; // Отметим выбранную (по-умолчанию, или выбрана пользователем)
            }
            // USER TYPES - U S E R   T Y P E S
            $this->arResult['PERSON_TYPES'][] = $ptype;
        }
        return $this->arResult;
    }


    /** Формирование arResult  Delivery + дополнительные услуги ( TITLE - PRICE )
     * @return mixed
     */
    protected function getDelivery4Result()
    {
        $this->arResult['SHIPMENT'] = [];

        $result = \Bitrix\Sale\Delivery\Services\Table::getList(array(
            'filter' => array('ACTIVE'=>'Y'),
        ));

        while($delivery=$result->fetch())
        {
            // выберем массив доп. услуг и включим к  массиву Доставки
            $extraServices = \Bitrix\Sale\Delivery\ExtraServices\Manager::getExtraServicesList($delivery['ID']);

            if ( !empty($extraServices) ) {
                foreach ($extraServices as $services)
                {
                    if ( $delivery['ID'] === $services['DELIVERY_ID']) {
                        $delivery['EXTRA_SERVICES'] =  $services['PARAMS']['PRICES'];
                    }
                }
            }
            $this->arResult['SHIPMENT'][] = $delivery;
        }
        return $this->arResult;
    }


    /**
     * Конфигурирование платежных систем
     */
    protected  function paymentProcess()
    {
        $paymentCollection = $this->order->getPaymentCollection();
        $payment = $paymentCollection->createItem();
        $payment->setField("SUM", $this->order->getPrice());
        // Если request добавляем Оплату к заказу
        if ( !empty($this->request['PAY_SYSTEM']) ) {
            $paySystemService = Sale\PaySystem\Manager::getObjectById(intval($this->request['PAY_SYSTEM']));
            $payment->setFields(array(
                'PAY_SYSTEM_ID' => $paySystemService->getField("ID"),
                'PAY_SYSTEM_NAME' => $paySystemService->getField("NAME"),
            ));
        }
        // Получение списка доступных платежных систем с учетом настроенных ограничений
        $arPaySystemServiceAll = Sale\PaySystem\Manager::getListWithRestrictions($payment);
        reset($arPaySystemServiceAll);
        $arPaySystem = current($arPaySystemServiceAll);
        if(!empty($arPaySystem)) {
            foreach ($arPaySystemServiceAll as  $k => $pay_system) {
                // отметив выбранную пользоваетелем
                if ($pay_system['ID'] == $this->request['PAY_SYSTEM']) {
                    $arPaySystemServiceAll[$k]['SELECTED'] = "Y";
                }
                if (!empty($pay_system['LOGOTIP'])) {
                    $arPaySystemServiceAll[$k]['LOGOTIP'] = CFile::GetPath($pay_system['LOGOTIP']);
                }
            }
        } else
            $payment->delete();

        // --- FOR RESULT
        $this->arResult['PAY_SYSTEMS'] = $arPaySystemServiceAll;
    }


    /**
     *  Конфигурирование Отгрузки и служб доставки + доп. услуг (доп платы за курьерскую доставку
     */
    protected function shipmentProcess()
    {
        $shipmentCollection = $this->order->getShipmentCollection();

        if (intval($this->request['DELIVERY_ID']) > 0) {
            $shipment = $shipmentCollection->createItem(
                Bitrix\Sale\Delivery\Services\Manager::getObjectById(
                    intval($this->request['DELIVERY_ID'])
                )
            );
        } else {
            $shipment = $shipmentCollection->createItem();
        }

        $shipmentItemCollection = $shipment->getShipmentItemCollection();
        $shipment->setField('CURRENCY', $this->order->getCurrency());
        $shipment->setField('CUSTOM_PRICE_DELIVERY', "Y");

        // Добавим стоимость доставки Курьером (если она передана)
        $shipment->setBasePriceDelivery(intval($this->request['EXTRA_SERVICES_PRICE']));

        if ( !empty($this->request['EXTRA_SERVICES_PRICE']) &&  $this->request['DELIVERY_ID']  == ID_DELIVERY_SHIPMENT) {
            $shipment->setField('PRICE_DELIVERY', intval($this->request['EXTRA_SERVICES_PRICE'])); // Стоимость доставки
            $shipment->setBasePriceDelivery(intval($this->request['EXTRA_SERVICES_PRICE']));
            // Добавим Название доставки и Пункт доставки (из  доп. услуг)
            if ( !empty($this->request['EXTRA_SERVICES']) ) {
                $name = "Доставка курьером, " . $this->request['EXTRA_SERVICES'];
                $shipment->setField('DELIVERY_NAME', $name ); // Стоимость доставки
            }
        }

        foreach ($this->order->getBasket()->getOrderableItems() as $item) {
          
            $shipmentItem = $shipmentItemCollection->createItem($item);
            $shipmentItem->setQuantity($item->getQuantity());
        }
    }


    /**
     * Работа с виртуальным заказом
     */
    protected function createVirtualOrder()
    {
        global $USER;

        $this->arResult = [];

        try {
            // Получим тип плательщтка (юзера) из request or result
            $this->setPersonType4Result();

            // Сформируем arResult  Способов Доставки
            $this->getDelivery4Result();

            //  А В Т О Р И З А Ц И Я  -  Р Е Г И С Т Р А Ц И Я
            // Узнаем ID Пользователя
            $registeredUserID = $USER->GetID();
            // Если пользователя нет - зарегистрируем
            if ($registeredUserID == 0 ) {
                // пробуем по емайл найти ID юзера
                // если не нашли - Регистрируем ( на лету )
                $arUser = $this->registerNewUser();
                if ( !empty($arUser) ) {
                    $registeredUserID = $arUser['ID'];
                }
                else {
//                    LocalRedirect("/auth/");
                }
            }

            // Р А Б О Т А   С   О Ф О Р М Л Е Н И Е М
            // Создаем заказ и привязываем корзину
            $this-> setOrderInBasket($registeredUserID);

            //  М Е Т О Д Ы   О П Л А Т Ы
            $this->paymentProcess();

            //  S H I P M E N T
            $this->shipmentProcess();

            //  Установка свойств
            $this->setOrderProps();

            // Сохраняем
            if ( !empty($this->request['PAY_SYSTEM']) ) {
                $this->order->doFinalAction(true);
                $result = $this->order->save();
                $orderId = $this->order->getId();

                if ( $result && $orderId ) {

                    $redirect_link = "https://".$_SERVER['SERVER_NAME']."/personal/order/?order_id=".$orderId;

                    $this->arResult['request']['order']['id'] = $orderId;
                    $this->arResult['request']['order']['request'] = $this->request;
                    $this->arResult['request']['order']['result'] = $this->arResult;
                    $this->arResult['request']['success']['result'] = "ok";
                    $this->arResult['request']['success']['redirect'] = $redirect_link;

                }
            }
            else {
                $this->arResult['request']['success']['result'] = "error";
            }

            return  $this->arResult;

        } catch (\Exception $e) {
            $this->errors[] = $e->getMessage();
        }
    }


    /**
     *  Пробуем найти по email , если не находим - регистрируем. Должно отправиться письмо на почтовый ящик юзера
     * @return array
     */
    protected function registerNewUser()
    {
        $arUser = [];

        //  Пробуем найти пользователя по ЕМАЙЛ
        if ( !empty($this->request['mail']) ) {
            $phone= str_replace(['-', '(', ')', '_',' '], '', $this->request['tel']);
            $phone= str_replace(['+'], '', $phone);
            $filter = Array("LOGIN" => $phone);
            $arUser = CUser::GetList([], ($order="desc"), $filter)->Fetch(); // выбираем пользователей

            if ($arUser) {
                return $arUser;
            }
            else {
                // ЕСЛИ НЕ НАХОДИМ - РЕГИСТРИРУЕМ
                $phone= str_replace(['-', '(', ')', '_',' '], '', $this->request['tel']);
                $phone= str_replace(['+'], '', $phone);
                $fio = $this->request['name'];
                $loggg = $phone;// Будущий логин
                $passs = randString(8, array("abcdefghijklnmopqrstuvwxyz","ABCDEFGHIJKLNMOPQRSTUVWX­YZ","0123456789","!@#",));// Генерирует пароль из 8-ми символов

                $arFields = Array(
                    "EMAIL"             => trim($this->request['mail']),// E-mail адрес, бывает важно чтобы он у каждого пользователя был уникален - проверьте свои настройки!
                    "LOGIN"             => $loggg,
                    "PHONE"=>$loggg,
                    "LAST_NAME"=>$fio,
                    "PERSONAL_PHONE"=>$loggg,
                    "ACTIVE"            => "Y",//Делаем пользователя активным
                    "GROUP_ID"          => array(2, 3,4, 5),//Id-групп пользователей сайта bitrix к которым принадлежит пользователь
                    "PASSWORD"          => $passs,
                    "CONFIRM_PASSWORD"  => $passs,
                );

                $user = new CUser;

                if ($id = $user->Add($arFields))
                {
                    $filter = Array("ID" => intval($id));
                    $arUser = CUser::GetList([], ($order="desc"), $filter)->Fetch(); // выбираем пользователей

                    if ( $arUser['ID'] == $id)  {
                        $arUser = $arUser;

                        $arEventFields= array(
                            "LOGIN" => $arUser["LOGIN"],
                            "NAME" => $this->request['name'],
                            "EMAIL" => trim($this->request['mail']),
                            "PASSWORD" => $passs,
                        );
                        CEvent::Send("NEW_USER_ORDER", SITE_ID, $arEventFields, "N");
                    }

                }
            }
        }
        return $arUser;
    }


    /***
     * Установка свойств заказа
     *  FIO | CITY | PHONE | EMAIL | ADDRESS | OFFER (Y/N) |
     */
    protected function setOrderProps()
    {
        global $USER;
        $arUser = $USER->GetByID(intval($USER->GetID()))
            ->Fetch();

        if (is_array($arUser)) {
            $fio = $arUser['LAST_NAME'] . ' ' . $arUser['NAME'] . ' ' . $arUser['SECOND_NAME'];
            $fio = trim($fio);
            $arUser['FIO'] = $fio;
        }
        $personType = $this->request['PERSON_TYPE_ID'];

        foreach ($this->order->getPropertyCollection() as $prop) {
          
            $value = '';
            switch ($prop->getField('CODE')) {
                case 'FIO':
                    $value = $this->request['name'];
                    $value = trim($value);
                    if (empty($value)) {
                        $value = $arUser['FIO'];
                    }
                    break;
                case 'CITY':
                    $value = $this->request['city'];
                    break;
                case 'EMAIL':
                    $value = $this->request['mail'];
                    break;
                case 'PHONE_UR':
                case 'PHONE':
                    $value = $this->request['tel'];
                    break;
                case 'ADDRESS_UR':
                    $value = $this->request['address'];
                    if($this->request['DELIVERY_ID'] ==1){
                        $value = $this->request['pickup_address'];
                    }
                    break;
                case 'ADDRESS':
                    $value = $this->request['address'];
                    if($this->request['DELIVERY_ID'] ==1){
                        $value = $this->request['pickup_address'];
                    }
                    break;
                case 'COMMENT':
                    $value = $this->request['comment'];
                    break;
                case 'COMPANY':
                    $value = $this->request['company_name'];
                    break;
                case 'CONTACT_PERSON':
                    $value = $this->request['company_fio'];
                    break;
                case 'UF_PREDSTAVITEL':
                    $value = $this->request['company_address'];
                    break;
                case 'INN':
                    $value = $this->request['company_inn'];
                    break;
                case 'EXPORT_DO':
                    $value = 'Y';
                    break;
                case 'OFFER':
                    if ($this->request['address'] == 'on') {
                        $value = 'Y';
                    }
                    break;
                default:
            }

            if (empty($value)) {
                foreach ($this->request as $key => $val) {
                    if (strtolower($key) == strtolower($prop->getField('CODE'))) {
                        $value = $val;
                    }
                }
            }

            if (empty($value)) {
                $value = $prop->getProperty()['DEFAULT_VALUE'];
            }

            if (!empty($value)) {
                $prop->setValue($value);
            }
        }

        $propertyCollection = $this->order->getPropertyCollection();

        foreach ($propertyCollection->getGroups() as $group) {
            foreach ($propertyCollection->getGroupProperties($group['ID']) as $property) {
                $p = $property->getProperty();
                if ($p["CODE"] == "CONTACT_PERSON")
                    $property->setValue($this->request['name']);
            }
        }


    }


}
